package model

import java.util.Date

import scala.collection.mutable.ListBuffer

/**
  * Created by mendoza on 15/04/2018.
  */
class MafiaOrganization {

  val activeGangsterList = ListBuffer[Gangster]()
  val imprisonedGangsterList = ListBuffer[Gangster]()

  private final val SPECIAL_SURVEILLANCE_CONDITION = 50

  //expected input name;date;boss => [String];[Long];[String]
  def loadMafiaRecords(input: List[String]) = {

    val inputTupleList = input.map(_.split(";")).map(x => (x(0), x(1), x(2)))
    inputTupleList.foreach(gangster => {
      //for simplicity, the Big boss is his own boss
      activeGangsterList += new Gangster(gangster._1, new Date(gangster._2.toLong), gangster._3, ListBuffer[Gangster](), null)
    })

    //loading subordinates
    inputTupleList.foreach(inputTuple => {
      if (inputTuple._1 != inputTuple._3) {
        val gangster = activeGangsterList.find(_.name == inputTuple._1).get
        val boss = activeGangsterList.find(_.name == inputTuple._3).get
        boss.subordinateList += gangster
      }
    })
  }

  def sendToJail(imprisonedGangsterName: String): Unit = {

    activeGangsterList.find(_.name == imprisonedGangsterName) match {
      case Some(imprisonedGangster) => {
        imprisonedGangster.jailEntry = new Date()
        imprisonedGangsterList += imprisonedGangster
        activeGangsterList -= imprisonedGangster

        if (!imprisonedGangster.subordinateList.isEmpty) {
          val sameRankGangster = activeGangsterList.find(gangster => gangster.bossName == imprisonedGangster.bossName && gangster.bossName != gangster.name)

          sameRankGangster match {
            case Some(newBoss) => {
              val newSubordinateList = imprisonedGangster.subordinateList.map(x => {

                x.bossName = newBoss.name
                x
              })
              newBoss.bossName = imprisonedGangster.bossName
              newBoss.subordinateList ++= newSubordinateList.filter(_ != newBoss)
            }
            case _ => {
              //we must promote a subordinate...
              val oldestSubordinate = activeGangsterList.find(_ == imprisonedGangster.subordinateList.minBy(_.date)).get
              oldestSubordinate.bossName = imprisonedGangster.bossName

              val newSubordinateList = imprisonedGangster.subordinateList.filter(_ != oldestSubordinate).map(x => {
                x.bossName = oldestSubordinate.name;
                x
              })
              oldestSubordinate.subordinateList ++= newSubordinateList

              activeGangsterList.find(gangster => gangster.name == oldestSubordinate.bossName) match {
                case Some(boss) => boss.subordinateList += oldestSubordinate
                case _ => None
              }

            }
          }
        }

        if (imprisonedGangster.bossName != imprisonedGangster.name) {
          activeGangsterList.find(_.name == imprisonedGangster.bossName) match {
            case Some(boss) => boss.subordinateList -= imprisonedGangster
            case _ => println("ERROR: CANNOT FIND BOSS FOR [" + imprisonedGangster.name + "]")
          }
        }

      }
      case _ => println("ERROR: GANGSTER [" + imprisonedGangsterName + "] NOT FOUND")
    }
  }

  def releaseFromJail(releasedGangsterName: String): Unit = {
    imprisonedGangsterList.find(_.name == releasedGangsterName) match {

      case Some(releasedGangster) => {
        if (!releasedGangster.subordinateList.isEmpty) {

          val activeDirectSubordinates = findCurrentActiveDirectSubordinates(releasedGangster)
          activeDirectSubordinates.foreach(subordinate => {
            activeGangsterList.find(x => x.name == subordinate.bossName) match {
              case Some(currentBoss) => currentBoss.subordinateList -= subordinate
              //subordinate took position big boss position
              case _ => None
            }
            subordinate.bossName = releasedGangster.name
          })

          releasedGangster.subordinateList = activeDirectSubordinates

        }
        val currentActiveBossName = findCurrentActiveBoss(releasedGangster.bossName)
        activeGangsterList.find(x => x.name == currentActiveBossName) match {
          case Some(gangster) => (gangster.subordinateList += releasedGangster )
          case _ => None
        }

        imprisonedGangsterList -= releasedGangster
        releasedGangster.jailEntry = null
        activeGangsterList += releasedGangster
      }
      case _ => println("ERROR: GANGSTER [" + releasedGangsterName + "] NOT IMPRISONED")
    }
  }

  private def findCurrentActiveDirectSubordinates(gangster: Gangster) : ListBuffer[Gangster] = {

    val currentActiveSubordinates = activeGangsterList.filter(x => gangster.subordinateList.contains(x))

    if (currentActiveSubordinates.isEmpty) {
      val imprisonedSubordinate = imprisonedGangsterList.filter(x => gangster.subordinateList.contains(x)).maxBy(_.jailEntry)

      if (imprisonedSubordinate.subordinateList.isEmpty) {
        imprisonedSubordinate.subordinateList
      } else {

        val subordinateNextInLine = imprisonedSubordinate.subordinateList.minBy(_.date)

        if (activeGangsterList.exists(x => x == subordinateNextInLine)) {
          ListBuffer[Gangster](subordinateNextInLine)
        } else {
          findCurrentActiveDirectSubordinates(subordinateNextInLine)
        }
      }
    } else {
      currentActiveSubordinates
    }
  }

  //when released, it may happen that former boss have gone to jail
  private def findCurrentActiveBoss(bossName: String): String = {
    activeGangsterList.find(_.name == bossName) match {
      case Some(boss) => boss.name
      case _ => {
        imprisonedGangsterList.find(prisoner => prisoner.name == bossName && prisoner.bossName != bossName) match {
          case Some(boss) => findCurrentActiveBoss(boss.bossName)
          case _ => bossName
        }
      }
    }
  }

  def underSpecialSurveillance(gangsterName: String): Boolean = {
    activeGangsterList.find(_.name == gangsterName) match {
      case Some(gangster) => {
        val ret = calculateGangstersUnderCommand(gangster)
        ret >= SPECIAL_SURVEILLANCE_CONDITION
      }
      case _ => {
        println("CANNOT FIND GANGSTER [" + gangsterName + "]")
        false
      }
    }
  }

  private def calculateGangstersUnderCommand(gangster: Gangster, gangsterKnownUnderCommand: Int = 0): Int = {

    val underCommand = gangster.subordinateList.size

    val subordinateList = gangster.subordinateList

    if (!subordinateList.isEmpty) {
      val total = for (subordinate <- subordinateList) yield {
        calculateGangstersUnderCommand(subordinate, underCommand)
      }
      total.sum + underCommand
    } else {
      underCommand
    }
  }
}