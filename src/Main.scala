/**
  * Created by mendoza on 15/04/2018.
  */

import model.MafiaOrganization

import scala.io.Source


object Main {


  def main(args: Array[String]): Unit = {

    val mo = new MafiaOrganization

    val fileStream = getClass.getResourceAsStream("/resource/input.txt")
    val inputList = Source.fromInputStream(fileStream).getLines.toList

    println("RECORDS COUNT: [" + inputList.size + "]")

    mo.loadMafiaRecords(inputList)

    println("CAPONE000 special survellaince [" + mo.underSpecialSurveillance("CAPONE000") + "]")
    println("CAPONE001 special survellaince [" + mo.underSpecialSurveillance("CAPONE001") + "]")
    println("CAPONE002 special survellaince [" + mo.underSpecialSurveillance("CAPONE002") + "]")
    println("CAPONE099 special survellaince [" + mo.underSpecialSurveillance("CAPONE099") + "]")

    mo.sendToJail("CAPONE005")
    mo.sendToJail("CAPONE006")

    mo.sendToJail("CAPONE002")
    mo.releaseFromJail("CAPONE005")
    mo.releaseFromJail("CAPONE006")

    println("END OF TEST")
    //end of test

  }

}
